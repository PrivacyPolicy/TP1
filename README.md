Respuestas

1) *

2) Deben ser bloques synchronized debido a que son secciones criticas y los hilos deben acceder a esas secciones de manera sincronica.

3) Los Hilos comparten recursos como el espacio de memoria, archivos abiertos y la situación de autenticación. Los hilos en ejecucion que comparten recursos se los llama proceso. El hecho de que compartan recursos hace que cualquiera de los hilos pueda modificarlos.

4)a) Un Thread puede instanciarse extendiendo de la clase Thread. Este metodo nos 	permite sobre-escribir el metodo run() ya que pertenece a la interface Runnable 	y Thread la implementa.
b) Un Thread puede instanciarse implementando la interfaz Runnable. En este caso implementamos el metodo run() en vez de sobre-escribirlo.
c) Utilizando el new() para crear el Hilo.