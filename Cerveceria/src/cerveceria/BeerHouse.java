/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cerveceria;

public class BeerHouse 
{
    //private int contenido;
    //private boolean contenedorlleno = Boolean.FALSE;
    private int stock = 0;
    
    /**
     * Obtiene de forma concurrente o síncrona el elemento que hay en el contenedor
     * @return Contenido el contenedor
     */
    public synchronized int get()
    {
        while (this.stock<=0)
        {
            try
            {
                wait();
            } 
            catch (InterruptedException e) 
            {
                System.err.println("Contenedor: Error en get -> " + e.getMessage());
            }
        }
        notify();
        //this.stock = this.stock - (int)((Math.random()*stock)+1)/5;
        int value = (int) ((Math.random()*5)+1);
        this.stock=this.stock-value;
        return value;
    }
 
    /**
     * Introduce de forma concurrente o síncrona un elemento en el contenedor
     * @param value Elemento a introducir en el contenedor
     */
    public synchronized int put(int value) 
    {
        while (stock>100) 
        {
            try
            {
                wait();
            } 
            catch (InterruptedException e) 
            {
                System.err.println("Contenedor: Error en put -> " + e.getMessage());
            }
        }
        stock = stock + value;
        notify();
        return this.stock;
    }
    public synchronized int cantidadActual(){
        
            return this.stock;
    }
}
