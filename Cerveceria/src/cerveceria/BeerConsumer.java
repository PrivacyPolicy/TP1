/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cerveceria;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BeerConsumer implements Runnable
{
    private final BeerHouse contenedor;
    private final int idconsumidor;
    private final int TIEMPOESPERA = 1500;

 
    /**
     * Constructor de la clase
     * @param contenedor Contenedor común a los consumidores y el productor
     * @param idconsumidor Identificador del consumidor
     */
    public BeerConsumer(BeerHouse contenedor, int idconsumidor) 
    {
        this.contenedor = contenedor;
        this.idconsumidor = idconsumidor;
    }
 
    @Override
    /**
     * Implementación de la hebra
     */
    public void run() 
    {
        while(Boolean.TRUE)
        {   

            try {
                System.out.println("El consumidor " + idconsumidor + " consume: " + contenedor.get());

                Thread.sleep(TIEMPOESPERA);
            } catch (InterruptedException ex) {
                Logger.getLogger(BeerConsumer.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}