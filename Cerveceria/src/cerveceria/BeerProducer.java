/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cerveceria;

import java.util.Random;

public class BeerProducer implements Runnable
{
    //private final Random aleatorio;
    private final BeerHouse contenedor;
    private final int idproductor;
    private final int TIEMPOESPERA = 1500;
 
    /**
     * Constructor de la clase
     * @param contenedor Contenedor común a los consumidores y el productor
     * @param idproductor Identificador del productor
     */
    public BeerProducer(BeerHouse contenedor, int idproductor) 
    {
        this.contenedor = contenedor;
        this.idproductor = idproductor;
        //aleatorio = new Random();
    }
 
    @Override
    /**
     * Implementación de la hebra
     */
    public void run() 
    {
        while(Boolean.TRUE)
        {   
            int value = 100-contenedor.cantidadActual();
            int poner = (int) ((Math.random()*value/2)+1);

            
            int stock=contenedor.put(poner);
            
            System.out.println("BeerHouse " + idproductor + " pone: " + poner + " stock: " + stock);
            try
            {
                Thread.sleep(TIEMPOESPERA);
            } 
            catch (InterruptedException e) 
            {
                System.err.println("BeerHouse " + idproductor + ": Error en run -> " + e.getMessage());
            }
        }
    }
}